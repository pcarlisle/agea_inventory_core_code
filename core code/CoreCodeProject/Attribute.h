#pragma once

#include <map>
#include <vector>
#include <string>

class TypeBase {
	public:
		TypeBase(void) { }
		virtual ~TypeBase(void) { }
};

template <class T>
class AnyType : public TypeBase {
	public:
		AnyType(T v) : value(v) { }
		~AnyType(void) { }

		T value;
};

class Attribute {
	private:
		std::map<std::size_t, std::map<std::string, TypeBase *>> m_values;

	public:
		Attribute(void) { }
		~Attribute(void) {
			for (auto &mapit : m_values) {
				for (auto &typeit : mapit.second) {
					delete typeit.second;
				}
			}
		}

		template <class T>
		void AddValue(std::string key, T value) {
			m_values[typeid(T).hash_code()].insert(std::make_pair(key, static_cast<TypeBase *>(new AnyType<T>(value))));
		}

		template <class T>
		T GetValue(std::string key) {
			auto typemap = m_values[typeid(T).hash_code()];
			auto it = typemap.find(key);

			if (it != typemap.end()) {
				return static_cast<AnyType<T> *>(it->second)->value;
			}

			return 0;
		}
};