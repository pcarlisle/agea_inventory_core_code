#pragma once

#include <string>
#include <map>

#include "Attribute.h"

class Item {
	public:
		std::size_t ID;
	
		std::string category;
		std::string name, longname;
		std::string description;
	
		std::size_t maxstack;
		std::size_t baseValue;
		float weight;

		std::map<std::string, Attribute *> attributes;

		std::string imagePath, meshPath;

		Item(void) = default;
		~Item(void) {
			for (auto it = attributes.begin(); it != attributes.end(); ++it) {
				delete (*it).second;
			}
		}
};

struct ItemStack {
	std::size_t ID;
	std::size_t count;
};