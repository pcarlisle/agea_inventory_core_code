#pragma once

#include <vector>

#include "Item.h"

class ItemManager;
class Inventory {
	private:
		std::vector<ItemStack *> m_items;

	public:
		Inventory(void) { };
		~Inventory(void) {
			for (auto &it : m_items) {
				delete it;
			}

			m_items.clear();
		};

		Inventory(std::vector<ItemStack *> items) { 
			m_items.insert(m_items.end(), items.begin(), items.end());
		}

		std::vector<ItemStack *> GetItemStacksWithID(std::size_t ID) {
			std::vector<ItemStack *> items;

			for (auto &it : m_items) {
				if (it->ID == ID) {
					items.push_back(it);
				}
			}

			return items;
		}

		const std::size_t AddItem(const std::size_t ID, const std::size_t count = 1) {
			const Item * item = ItemManager::Instance()->GetItemByID(ID);

			if (!item) { return 0;  }
			
			std::size_t added = 0, maxstack = item->maxstack;

			if (!maxstack) { maxstack = 0; }

			for (auto it = m_items.begin(); it != m_items.end(); ++it) {
				if ((*it)->ID == ID) {
					if ((*it)->count + (count - added) <= maxstack) {
						added += (*it)->count;
						(*it)->count += (count - added);
					}
				}

				if (added == count) { break; }
			}
			
			while (added < count) {
				std::size_t n = ((count - added) > maxstack ? maxstack : (count - added));
				m_items.push_back(new ItemStack({ ID, n }));
				added += n;
			}

			return added;
		}

		const std::size_t RemoveItem(const std::size_t ID, const std::size_t count = 1) {
			if (!count) { return 0; }

			std::size_t removed = 0;

			for (auto it = m_items.begin(); it != m_items.end(); ++it) {
				if ((*it)->ID == ID) {
					if ((*it)->count > (count - removed)) {
						(*it)->count -= (count - removed);
						removed = count;
						break;
					} else if ((*it)->count < (count - removed)) {
						removed += (*it)->count;
						delete (*it);
						it = m_items.erase(it);
					} else {
						removed = count;
						delete (*it);
						it = m_items.erase(it);
						break;
					}
				}
			}

			return removed;
		}

		bool RemoveItemStack(ItemStack * item) {
			for (auto it = m_items.begin(); it != m_items.end(); ++it) {
				if ((*it) == item) {
					delete (*it);
					m_items.erase(it);
					return true;
				}
			}

			return false;
		}

		bool AddInventory(Inventory * inventory) {
			if (this == inventory) { return false; }

			const std::vector<ItemStack *> items = inventory->GetAllItemStacks();
			m_items.insert(m_items.end(), items.begin(), items.end());
			return true;
		}

		const std::vector<ItemStack *> & GetAllItemStacks(void) { return m_items; }
};