#pragma once

#include "Item.h"

class ItemManager {
	private:
		static ItemManager * m_instance;

		std::vector<Item *> m_items;

	public:
		ItemManager(void) { }
		~ItemManager(void) { }

		static ItemManager * Instance(void) {
			if (!m_instance) {
				m_instance = new ItemManager();
			}

			return m_instance;
		}

		void Shutdown(void) {
			for (auto it = m_items.begin(); it != m_items.end(); ++it) {
				delete (*it);
				it = m_items.erase(it);
			}

			m_items.clear();
		}

		void AddNewItem(Item * item) {
			m_items.push_back(item);
		}

		const Item * GetItemByID(std::size_t ID) {
			for (auto it = m_items.begin(); it != m_items.end(); ++it) {
				if ((*it)->ID == ID) {
					return *it;
				}
			}

			return 0;
		}
};