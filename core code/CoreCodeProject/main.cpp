#include <stdio.h>
#include <string>

#include "Item.h"
#include "ItemManager.h"
#include "Inventory.h"

int main(int argc, char * argv[]) {
	// Example items
	Item * itemA = new Item({
		0,
		"Potions",
		"Health", "Health Potion",
		"This item gives you health",
		64, 8, 0.1f,
		std::map<std::string, Attribute *>({ 
			{ "A", new Attribute() }, 
			{ "B", new Attribute() } 
		}),
		"images/potions/health.png",
		"meshes/potions/health.mesh"
	});

	Item * itemB = new Item({
		1,
		"Potions",
		"Mana", "Mana Potion",
		"This item gives you mana",
		32, 8, 0.1f,
		std::map<std::string, Attribute *>({
			{ "C", new Attribute() },
			{ "D", new Attribute() } 
		}),
		"images/potions/mana.png",
		"meshes/potions/mana.mesh"
	});

	// Adding example attribute values
	itemA->attributes["SwordInfo"] = new Attribute();
	itemA->attributes["SwordInfo"]->AddValue("Damage", 50);
	itemA->attributes["SwordInfo"]->AddValue("Weight", 5.054f);
	itemA->attributes["SwordInfo"]->AddValue("Rarity", 3U);
	itemA->attributes["SwordInfo"]->AddValue("Name", "John");

	// Accessing example attribute values
	int damage = itemA->attributes["SwordInfo"]->GetValue<int>("Damage");
	float weight = itemA->attributes["SwordInfo"]->GetValue<float>("Weight");
	unsigned int rarity = itemA->attributes["SwordInfo"]->GetValue<unsigned int>("Rarity");
	std::string name = itemA->attributes["SwordInfo"]->GetValue<const char *>("Name");

	// Adding the items to the manager
	ItemManager::Instance()->AddNewItem(itemA);
	ItemManager::Instance()->AddNewItem(itemB);

	// Adding items to an inventory
	Inventory inv; 
	inv.AddItem(0);
	inv.AddItem(1, 546);

	// Removing items from an inventory
	inv.RemoveItem(1, 200);
	inv.RemoveItem(1, 20);

	// Obtaining stacks with that ID
	std::vector<ItemStack *> items = inv.GetItemStacksWithID(1);
	ItemStack * stack = items.front();

	// Can't add inventories to themselves
	inv.AddInventory(&inv);

	ItemManager::Instance()->Shutdown();
	delete ItemManager::Instance();

	return 0;
}